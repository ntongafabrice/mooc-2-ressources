Thématique : HTML et CSS 

Notions liées : Aucun pré-requis.

Résumé de l’activité :  Deux TP permettant de découvrir des langages du web.

TP 1 : observation, modification, construction de pages web sous forme de questions.

Structure d’une page Web, structure d’une table, les liens, images, sons, les listes. 

TP 2 : observation, modification, construction de pages web sous forme de questions.

Fichier CSS. Corriger les erreurs d’un site web.

Objectifs :  Découverte des langages HTML et CSS.

Auteur : Charles Poulmaire charles.poulmaire@ac-versailles.fr

Durée de l’activité : 2 * 2h

Forme de participation : individuelle ou en binôme, en autonomie, collective.

Matériel nécessaire : Papier stylo et ordinateur !

Préparation : Aucune

Références:

Fiche élève cours : Après les deux activités

Fiche élève activité :
  
Déroulé : 
